#include "RtPlot.hpp"
#include <iostream>

#include "RtMainFrame.hpp"
#include "reader.hpp"
#include "object.hpp"
#include "parser.hpp"
#include "exceptions.hpp"



static const char* dummy_argv[] = {"rt-plot"};
static int         dummy_argc   = 1;


RtPlot::RtPlot(bool verbose, bool batch, bool abort) :
    TApplication( "rt-plot", &dummy_argc, const_cast<char**>( dummy_argv ) ),
    m_reader(    new LineReader(STDIN_FILENO) ),
    m_parser(    new Parser ),
    m_fdWatcher( new TFileHandler(STDIN_FILENO , TFileHandler::kRead) ),
    m_verbose( verbose ),
    m_batch  ( batch   ),
    m_abort  ( abort   )
{
    // Set up notification
    m_fdWatcher->Add();
    TQObject::Connect(m_fdWatcher, "Notified()",
                      "RtPlot", this, "readMoreData()");
    // Create canvas to draw upon
    if( getenv("DISPLAY") == 0 ) {
        // No graphics
        TCanvas* cnv = new TCanvas;
        m_plot = new Plot( cnv );
    } else {
        // We have X. Let create window
        RtMainFrame* bp = new RtMainFrame( gClient->GetRoot() );
        m_plot = new Plot( bp->getCanvas() );
        bp->setPlot( m_plot );
    }
}

RtPlot::~RtPlot()
{
    delete m_fdWatcher;
    delete m_parser;
    delete m_reader;
    delete m_plot;
}

void RtPlot::readMoreData() {
    // No more data from stdin
    if( m_reader->eof() ) {
        if( m_batch ) {
            gApplication->Terminate();
        }
        delete m_fdWatcher;
        m_fdWatcher = 0;
    }

    std::string str;
    while( m_reader->getLine( str ) == LineReader::OK ) {
        if( m_verbose )
            std::cout << "> '" << str << "'\n";
        RtError err = m_parser->feedLine( m_plot, str );
        if( !err.clean() ) {
            std::cerr << "rt-plot: " << err.error() << '\n';
            if( m_abort )
                Terminate(1);
            m_plot->reportError( err );
        }
    }
}
