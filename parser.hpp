
#ifndef RT_ROOT_PARSER__HPP__
#define RT_ROOT_PARSER__HPP__

#include <string>
#include <vector>
#include <iostream>

#include <boost/variant.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/scoped_ptr.hpp>

#include "exceptions.hpp"


// ================================================================

class Plot;
class Pad;
class Parser;

// Token of the language
typedef boost::variant<int, double, std::string> Token;
// Define token type for bison
#define YYSTYPE Token

// Parameters for bison parser
struct ParseParam {
    ParseParam(Parser* par, Plot* p) :
        parser   (par),
        plot     (p),
        clearPlot(false),
        axis     (0),
        onOff    (true),
        pad(0)
    {}
    
    Parser*    parser;          // Pointer to parser
    Plot*      plot;            // Pointer to plot object
    // ==== Locals ================
    bool       clearPlot;       // Should we clear plot
    int        axis;            // Axis
    bool       onOff;           // ON/OFF flag

    // Hack for dealing with 
    Pad*       pad;             // Pointer to pad
    // Sets pad to current pad and returns true if it's not null
    bool operator()();
};
int yyparse(ParseParam);



// ================================================================

// Incremental parser for embedded data. E.g. graphs, histograms etc.
class LineAccum {
public:
    LineAccum() : ok(true) {}
    virtual ~LineAccum() {}

    // If no errors were encountered during parsing it adds its
    // contents to the plot and returns true. Otherwise just returns
    // false and do nothing to Plot.
    bool flush(Plot* plot);
    // Feed line to accumulator. After first parse error it becomes a no op
    bool feedLine(const std::string& str);
    // Reads from data from file line by list name and flushes result
    // into plot if succeeds.
    bool readFromFile(const std::string& fname, Plot* plot);
protected:
    // Worker functions which actually do all the work. Derived
    // classes should not bother about entering invalid state. After
    // first failure none of the methods will be called.
    virtual bool doFlush   (Plot* plot)             = 0;
    virtual bool doFeedLine(const std::string& str) = 0;
private:
    bool ok;
};



// Pointer to the line accumulator. It's always used in shared_ptr so
// it's possible to allocate.
typedef boost::shared_ptr<LineAccum> PLineAccum;



// Allocate accumulator for graphs
PLineAccum makeAccumGraph();

// Allocate accumulator for 2D graphs
PLineAccum makeAccumGraph2D();

// Allocate accumulator for polygons
PLineAccum makeAccumPoly();

// Allocate accumulator for barchart
PLineAccum makeAccumBarchart();

// Allocate accumulator for polygons
PLineAccum makeAccumHist();




// ================================================================

// Line parser. It's main parser class and it drops to the LineAccum
// when needed.
class Parser {
public:
    // Construct parser
    Parser();

    // Feed line to the parser. Returns value of Error class which
    // could be either empty (no error occured) or contain actual
    // error.
    RtError feedLine(Plot* plot, const std::string& str);
    // Set accumulator forinline data
    void accumulate(PLineAccum a) { m_accum = a; }
private:
    // Pointer to current accumulator
    boost::shared_ptr<LineAccum> m_accum;
};

#endif /* RT_ROOT_PARSER__HPP__ */
