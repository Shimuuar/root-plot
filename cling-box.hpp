#ifndef RT_ROOT_CLING_BOX__HPP__
#define RT_ROOT_CLING_BOX__HPP__

// CINT of ROOT 6 cannot tolerate incomplete type definitions for some
// reason. We have to fool him with a box
template<typename T>
struct Box {
    Box()     : box(0) {}
    Box(T* x) : box(x) {}
    operator T*() { return box; }
    Box<T>& operator= (T* x) { box = x; return *this; }
    T*      operator->()     { return box; }

    T* box;
};

#endif /* RT_ROOT_CLING_BOX__HPP__ */
