
#include "RtPlot.hpp"

#include <iostream>
#include <cstdlib>
#include <signal.h>

#include <TH1.h>
#include <TROOT.h>
#include <TStyle.h>


void usage() {
    std::cerr <<
        "Usage:\n"
        "    rt-plot [flags]\n"
        "\n"
        "Reads commands from standard input and creates a plot\n"
        "\n"
        "    -b - batch mode. terminate after end of input\n"
        "    -x - do not create window\n"
        "    -e - exit on first error\n"
        "    -v - verbose. Echo input to stdout\n"
        "    -h - this message\n"
        ;
    std::exit(1);
}

int main(int argc, char** argv)
{
    // Uninstall SIGSEVG handler
    signal( SIGSEGV, SIG_DFL );
    // Do not histograms to current dir
    TH1::AddDirectory(kFALSE);
    // Reasonable color schemes
    gROOT->SetStyle("Plain");

    // Parse command line parameters
    bool verbose     = false;
    bool batch       = false;
    bool stop_on_err = false;
    bool no_X        = false;
    for( int c; ((c = getopt (argc, argv, "hxvbe")) != -1); ) {
        switch(c) {
        case 'x':
            no_X = true;
            break;
        case 'b':
            batch = true;
            break;
        case 'v':
            verbose = true;
            break;
        case 'e':
            stop_on_err = true;
            break;
        case 'h':
        default :
            usage();
            break;
        }
    }

    // We don't want graphics so we have to unset display.
    // Otherwise ROOT will create windows
    //
    // That worked with ROO5 but ROOT6 become wicked and tries to set
    // DISPLAY to something if we don't have it set. Why? I don't have
    // slightest idea. We need to put it into batch mode
    if( no_X ) {
        gROOT->SetBatch();
        if( -1 == unsetenv("DISPLAY") ) {
            perror("rt-plot: unable to unset DISPLAY");
            exit(1);
        }
    }

    RtPlot app( verbose, batch, stop_on_err );
    app.Run();
    return 0;
}
