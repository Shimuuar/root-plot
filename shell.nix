let pkgs = import <nixpkgs> {};
in
  pkgs.stdenv.mkDerivation {
    name        = "root-plot-env";
    buildInputs = with pkgs; [
      root
      bison
      flex
      boost
      ];
  }
