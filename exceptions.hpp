#ifndef RT_PLOT_EXCEPTIOS__HPP__
#define RT_PLOT_EXCEPTIOS__HPP__

#include <stdexcept>
#include <string>
#include <boost/shared_ptr.hpp>

// Error during parsing
class ParseError : public std::runtime_error
{
public:
    ParseError(const std::string& str) : std::runtime_error(str) {}
};

// Error which could occure during decoding
class RtError {
public:
    // Null error which isn't an error actually
    RtError();
    // Create error with custom message
    RtError(const std::string& msg);

    // Returns true if there's no error
    bool clean();
    // String represenation of error
    std::string error();
private:
    boost::shared_ptr<std::string> m_msg;
};


#endif /* RT_PLOT_EXCEPTIOS__HPP__ */
