#ifndef RT_ROOT_RTPLOT__HPP__
#define RT_ROOT_RTPLOT__HPP__

#include <TApplication.h>
#include <TSysEvtHandler.h>
#include <RQ_OBJECT.h>
#include "cling-box.hpp"

class Parser;
class LineReader;
class Plot;

// Main application for rt-plot
class RtPlot : public TApplication {
    RQ_OBJECT()
public:
    RtPlot(bool verbose, bool batch, bool abort);
    virtual ~RtPlot();

    // Read more data from standard input
    void readMoreData();
private:
    Box<LineReader>  m_reader;       // Reader for stdin
    Box<Parser>      m_parser;       // Parser
    Box<Plot>        m_plot;         // Plot to draw on
    TFileHandler* m_fdWatcher;    // Notify when there is data to read
    bool          m_verbose;      // Verbose mode flag
    bool          m_batch;        // Batch mode flag
    bool          m_abort;        // Abort on error
public:
    // CINT stuff
    ClassDef(RtPlot,1);
    // void Streamer(TBuffer&) {};
private:

};


#endif /* RT_ROOT_RTPLOT__HPP__ */
