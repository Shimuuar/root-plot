import Criterion.Main


import Data.Histogram.Fill
import HEP.ROOT.Plot
import HEP.ROOT.Plot.AST

smallHist = fillBuilder (forceInt -<< mkSimple (binD 0 100 1))                 []
bigHist   = fillBuilder (forceInt -<< mkSimple (binD 0 100 1 >< binD 0 100 1)) []

main :: IO ()
main = defaultMain
 [ bgroup "network"
   [ bench "small hist" $ sendCommands (add $ hist smallHist)
   , bench "big hist"   $ sendCommands (add $ hist bigHist  )
   ]
 , bgroup "local"
   [ bench "small hist" $ nfIO (renderCommands (add $ hist smallHist))
   , bench "big hist"   $ nfIO (renderCommands (add $ hist bigHist  ))
   ]
 ]
