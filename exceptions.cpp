#include "exceptions.hpp"
#include <boost/make_shared.hpp>
#include <iostream>


RtError::RtError()
{}

RtError::RtError(const std::string& s) :
    m_msg( boost::make_shared<std::string>(s) )
{}

bool RtError::clean() {
    return 0 == m_msg.get();
}

std::string RtError::error() {
    return  m_msg ? *m_msg : "";
}
